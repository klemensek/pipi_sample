# pipi sample project

## installation:

### 1. create database
- copy sample.conf.db.php to conf.db.php
- copy sample.config.php to config.php
fix db connection in config.php

R::freeze(true) - no on the fly tables/fields will be created 

R::freeze(false) - tables/fields will be created on the fly 


### 1. composer


### 2. run:

- mysql restore (pipisample.sql)



### 3. 
Sortiranje podatkov avtomobilov po:

- oddaljenosti od Celovska cesta 34

- ceni

- ratingu

Gumbi/drop down za sortiranje na spletni strani in pa implementacija sortiranja v backendu.





### sample vhost:


<VirtualHost *:80>
	ServerName www.pipisample.si
	DocumentRoot C:/wamp64/www/pipi_sample
	SetEnv APPLICATION_ENV "www.pipisample.si"
	<Directory  "C:/wamp64/www/pipi_sample">
		DirectoryIndex index.php
		AllowOverride all
		Order Allow,Deny
		Allow from all
	</Directory>
</VirtualHost>

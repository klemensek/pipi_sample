/*------------------------------------------------------
    Author : www.webthemez.com
    License: Commons Attribution 3.0
    http://creativecommons.org/licenses/by/3.0/
---------------------------------------------------------  */

function confirmDelete(id) {
    if (!confirm("Do you want to delete")){
        return false;
    }

    $("#formSection #delete").val(id);
    $("#formSection").submit();
    return false;
}

(function ($) {
    "use strict";
    var mainApp = {

        initFunction: function () {
            /*MENU 
            ------------------------------------*/
            $('#main-menu').metisMenu();
			
            $(window).bind("load resize", function () {
                if ($(this).width() < 768) {
                    $('div.sidebar-collapse').addClass('collapse')
                } else {
                    $('div.sidebar-collapse').removeClass('collapse')
                }
            });

        },

        initialization: function () {
            mainApp.initFunction();

        }

    }
    // Initializing ///

    $(document).ready(function () {
        mainApp.initFunction(); 
		$("#sideNav").click(function(){
			if($(this).hasClass('closed')){
				$('.navbar-side').animate({left: '0px'});
				$(this).removeClass('closed');
				$('#page-wrapper').animate({'margin-left' : '260px'});
				
			}
			else{
			    $(this).addClass('closed');
				$('.navbar-side').animate({left: '-260px'});
				$('#page-wrapper').animate({'margin-left' : '0px'}); 
			}
		});

        $(".delete").click(function(){
            confirmDelete($(this).data("id"));
        });

        $('#dataTables-example').dataTable();

        var dateSettings = {
            format: 'YYYY-MM-DD',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down",
                previous: "fa fa-arrow-left",
                next: "fa fa-arrow-right"
            }
        };
        $('#datehired').datetimepicker(dateSettings);
        $('#dateofcompletion').datetimepicker(dateSettings);
        $('#datecompletion_ot_1').datetimepicker(dateSettings);
        $('#datecompletion_ot_2').datetimepicker(dateSettings);
        $('#datecompletion_ot_3').datetimepicker(dateSettings);
        $('#datecompletion_ot_4').datetimepicker(dateSettings);

    });

}(jQuery));

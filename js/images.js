
'use strict';

;( function( $, window, document, undefined )
{
    // feature detection for drag&drop upload

    var isAdvancedUpload = function()
    {
        var div = document.createElement( 'div' );
        return ( ( 'draggable' in div ) || ( 'ondragstart' in div && 'ondrop' in div ) ) && 'FormData' in window && 'FileReader' in window;
    }();


    // applying the effect for every form

    $( '.box' ).each( function()
    {
        var $form		 = $( this ),
            $input		 = $form.find( '.imageUploader input[type="file"]' ),
            $label		 = $form.find( '.imageUploader label' ),
            $errorMsg	 = $form.find( '.imageUploader .box__error span' ),
            $restart	 = $form.find( '.imageUploader .box__restart' ),
            droppedFiles = false,
            showFiles	 = function( files )
            {
                //$label.text( files.length > 1 ? ( $input.attr( 'data-multiple-caption' ) || '' ).replace( '{count}', files.length ) : files[ 0 ].name );
            };

        // letting the server side to know we are going to make an Ajax request
        $form.append( '<input type="hidden" name="ajax" value="1" />' );

        // automatically submit the form on file select
        $input.on( 'change', function( e )
        {
            showFiles( e.target.files );


            // $form.trigger( 'submit' );
            formimageUpload();

        });


        // drag&drop files if the feature is available
        if( isAdvancedUpload )
        {
            $form
                .addClass( 'has-advanced-upload' ) // letting the CSS part to know drag&drop is supported by the browser
                .on( 'drag dragstart dragend dragover dragenter dragleave drop', function( e )
                {
                    // preventing the unwanted behaviours
                    e.preventDefault();
                    e.stopPropagation();
                })
                .on( 'dragover dragenter', function() //
                {
                    $form.addClass( 'is-dragover' );
                })
                .on( 'dragleave dragend drop', function()
                {
                    $form.removeClass( 'is-dragover' );
                })
                .on( 'drop', function( e )
                {
                    droppedFiles = e.originalEvent.dataTransfer.files; // the files that were dropped
                    showFiles( droppedFiles );


                    // $form.trigger( 'submit' ); // automatically submit the form on file drop
                    formimageUpload();

                });
        }


        // if the form was submitted

        var formimageUpload = function () {


        //$form.on( 'submit', function( e )        {
            // preventing the duplicate submissions if the current one is in progress
            if( $form.hasClass( 'is-uploading' ) ) return false;

            $form.addClass( 'is-uploading' ).removeClass( 'is-error' );

            if( isAdvancedUpload ) // ajax file upload for modern browsers
            {
                //e.preventDefault();

                // gathering the form data
                var ajaxData = new FormData( $form.get( 0 ) );
                if( droppedFiles )
                {
                    $.each( droppedFiles, function( i, file )
                    {
                        ajaxData.append( $input.attr( 'name' ), file );
                    });
                }

                // ajax request
                $.ajax(
                    {
                        url: 			'/savetofile.php',
                        type:			$form.attr( 'method' ),
                        data: 			ajaxData,
                        dataType:		'json',
                        cache:			false,
                        contentType:	false,
                        processData:	false,
                        complete: function()
                        {
                            $form.removeClass( 'is-uploading' );
                        },
                        success: function( data )
                        {


                            $.each(data.files, function (index, file) {
                                if (file.thumbnailUrl) {


                                    var img = $("<img />");
                                    //img.attr("width", 150);
                                    img.attr("src", file.thumbnailUrl);
                                    var icon = $("<i />");
                                    icon.attr('class', 'fa fa-trash-o');

                                    file.deleteUrl = file.deleteUrl + '&delete_id=' + file.id;

                                    var link = $('<a>')
                                        .attr('target', '_blank')
                                        .attr('class', 'delete')
                                        .prop('href', "#")
                                        .attr('delete', file.deleteUrl);
                                    img
                                        .wrap(link);

                                    link.append(icon);
                                    link.append(img);

                                    link.appendTo(".imageUploaderimages");

                                } else if (file.error) {
                                    var $context = $('<div/>').appendTo('.imageUploaderimages');
                                    var error = $('<span class="text-danger"/>').text(file.error);
                                    $($context.children()[index])
                                        .append('<br>')
                                        .append(error);
                                }
                            });

                            // for (var x = 0; x < data.files.length; x++) {
                            //     var img = $("<img />");
                            //     img.attr("width", 150);
                            //     img.attr("src", data.files[x].url);
                            //     $(img).appendTo(".imageUploaderimages");
                            // }

                            $form.addClass( data.files ? 'is-success' : 'is-error' );
                            if( !data.files ) {
                                $.each(data.files, function (index) {
                                    var error = $('<span class="text-danger"/>').text('File upload failed.');
                                    $($context.children()[index])
                                        .append('<br>')
                                        .append(error);
                                });
                            };
                        },
                        error: function()
                        {

                        }
                    });
            }
            else // fallback Ajax solution upload for older browsers
            {
                var iframeName	= 'uploadiframe' + new Date().getTime(),
                    $iframe		= $( '<iframe name="' + iframeName + '" style="display: none;"></iframe>' );

                $( 'body' ).append( $iframe );
                $form.attr( 'target', iframeName );

                $iframe.one( 'load', function()
                {
                    var data = $.parseJSON( $iframe.contents().find( 'body' ).text() );
                    $form.removeClass( 'is-uploading' ).addClass( data.success == true ? 'is-success' : 'is-error' ).removeAttr( 'target' );
                    if( !data.success ) $errorMsg.text( data.error );
                    $iframe.remove();
                });
            }
        // });
        };


        // restart the form if has a state of error/success

        $restart.on( 'click', function( e )
        {
            e.preventDefault();
            $form.removeClass( 'is-error is-success' );
            $input.trigger( 'click' );
        });

        // Firefox focus bug fix for file input
        $input
            .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
            .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });


        $(".imageUploaderimages").on("click", "a.delete", function () {

            var base = $(this);
            var $link = $(this).attr("delete");

            if(confirm("Delete ? ")){
                $.ajax({
                    url: $link,
                    type: 'DELETE',
                    success: function(result) {
                        base.remove();
                    }
                });

            }
            return false;
        });

    });

})( jQuery, window, document );
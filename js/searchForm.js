function load_dropdown(lang, default_val, id, type, pre_val, language) {
    $.ajax({
        type: "GET",
        url: SITE_URL + "/form/common/ota.php", cache: false, timeout: AJAX_TIMEOUT,
        beforeSend: function () {
            $("#" + id + "_" + type).html('<option value="">' + LOADING + '</option>');
        },
        data: "type=" + type + "&lang=" + lang + "&default_val=" + default_val + "&pre_val=" + pre_val + "&language=" + language,
        success: function (msg) {
            $("#" + id + "_" + type).html(msg);
            $("#" + id + "_" + type).find('option:selected').addClass('active');
            $("#" + id + "_" + type).change(function () {
                $("#" + id + "_" + type).find('option').removeClass('active');
                $("#" + id + "_" + type).find('option:selected').addClass('active');
            });
        },
        statusCode: {
            502: function () {
                load_dropdown(lang, default_val, id, type, pre_val, language);
            },
            504: function () {
                load_dropdown(lang, default_val, id, type, pre_val, language);
            }
        },
        error: function (x, t, m) {
            if (t === "timeout") {
                load_dropdown(lang, default_val, id, type, pre_val, language);
            }
        }
    });
}

function reset_dropdown(id, type) {
    $("#" + id + "_" + type).html('<option value="">' + SELECT_LOCATION + '</option>');
}

function load_time(default_val, id) {
    $.ajax({
        type: "GET",
        url: SITE_URL + "/form/common/ota.php", cache: false, timeout: AJAX_TIMEOUT,
        beforeSend: function () {
        },
        data: "type=time&default_val=" + default_val + "&id=" + id,
        success: function (msg) {
            $("#time_" + id).html(msg);
            $("#time_" + id).find('option:selected').addClass('active');
            $("#time_" + id).change(function () {
                $("#time_" + id).find('option').removeClass('active');
                $("#time_" + id).find('option:selected').addClass('active');
            });
        },
        statusCode: {
            502: function () {
                load_time(default_val, id);
            },
            504: function () {
                load_time(default_val, id);
            }
        },
        error: function (x, t, m) {
            if (t === "timeout") {
                load_time(default_val, id);
            }
        }
    });
}


$(document).ready(function () {

    $(".pickdate").datepicker({
        defaultDate: "+1d",
        minDate: 0,
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 2,
        onSelect: function (selectedDate) {
            var a = new Array;
            var b = "";
            var c = "";
            var d = "";
            var e = "";
            var date = new Date();
            var f = date.getFullYear();
            c = selectedDate;
            a = c.split("-");
            d = a[1] + "/" + a[2] + "/" + a[0];
            var g = new Date(d);
            g.setDate(g.getDate() + 1);
            e = g.getMonth() + 1;
            b = g.getFullYear() + "-" + ("0" + e).slice(-2) + "-" + ("0" + g.getDate()).slice(-2);
            if (!isNaN(g.getDate()))
                $(".dropdate").datepicker("option", "minDate", b || '2013-10-10');
            $(".dropdate").val(b);
        }
    });
    $(".dropdate").datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        numberOfMonths: 2,
        onSelect: function (selectedDate) {
            //	$( "#dropdate" ).datepicker( "option", "maxDate", selectedDate || '2013-10-10');
        }
    });


    // if (predefined_pickup_time) {
    //     load_time(predefined_pickup_time, 'pickup');
    // } else {
    //     load_time('10:00', 'pickup');
    // }
    // if (predefined_dropoff_time) {
    //     load_time(predefined_dropoff_time, 'dropoff');
    // } else {
    //     load_time('10:00', 'dropoff');
    // }
});

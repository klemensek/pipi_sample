<?php

$p404 = false;
$content = "";

$path = handleUrlPath();

$tpl = new template();
$tpl->set("path", $path);
$tpl->set("menu", $tpl->fetch('view/base/menu.php'));

function displayHeaderAndNav($tpl)
{
    $head = $tpl->fetch('view/base/head.php');
    echo $head;

    $nav = $tpl->fetch('view/base/nav.php');
    echo $nav;
}

function displayFooter($tpl)
{
    $tpl->whereami = substr($_SERVER["REQUEST_URI"], 1, strlen($_SERVER["REQUEST_URI"]));
    $foot = $tpl->fetch('view/base/foot.php');
    echo $foot;

}


$router = new minirouter($path);
/**/
$router->a('/', function ($tpl) {

    displayHeaderAndNav($tpl);

    $content = $tpl->fetch('view/hp.php');
    echo $content;
});

$router->a('', function ($tpl) {

    displayHeaderAndNav($tpl);
    $content = $tpl->fetch('view/404.php');
    echo $content;
});



$router->a(_t('link_search'), function ($tpl) {

    $params = $_GET;

    $results = searchListings($tpl, $params);
    $tpl->set("results", $results);
    $tpl->set("params", $params);

    displayHeaderAndNav($tpl);

    $tpl->set("scriptType", "SearchListing");
    $content = $tpl->fetch('view/listing/search.php');
    echo $content;
});


$router->e($tpl);

displayFooter($tpl);


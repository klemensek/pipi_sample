<?php



function prepareLastListings($tpl){


    $data = R::findAll('listing', 'active = 1 order by id asc');
    foreach ($data as $item) {
        $images = R::findAll('images', ' listing_id = ? order by id asc limit 1', array($item->id));
        foreach ($images as $image) {
            $item->images = $image->export();
        }

        $reviews = R::findAll('review', ' active = 1 and listing_id = ? order by id asc', array($item->id));
        $suma = 0;
        foreach ($reviews as $review) {
            $suma = $suma + $review->rating;
        }
        if($suma>0){
            $item->rating = ceil($suma / count($reviews));
        }else{
            $item->rating = 0;
        }
    }

    $tpl->set('lastListings', $data);
    $content = $tpl->fetch('view/hp_carousel.php');

    return $content;

}

function searchListings($tpl, $params){

    $data = array();


    $title = (isset($params["all"])) ? $params["all"] : "";
    $datefrom = (isset($params["datefrom"])) ? $params["datefrom"] : "";
    $dateto = (isset($params["dateto"])) ? $params["dateto"] : "";
    $location = (isset($params["location"])) ? $params["location"] : "";



    $data = R::findAll('listing', ' 
    title like ? 
    and address like ? 
    and active = 1
    ', array(
        "%$title%",
        //"%$datefrom%",
        //"%$dateto%",
        "%$location%"
    ));



    foreach ($data as $item) {
        $images = R::findAll('images', ' listing_id = ? order by id asc limit 1', array($item->id));
        foreach ($images as $image) {
            $item->images = $image->export();
        }


        $reviews = R::findAll('review', ' active = 1 and listing_id = ? order by id asc', array($item->id));
        $suma = 0;
        foreach ($reviews as $review) {
            $suma = $suma + $review->rating;
        }
        if($suma>0){
            $item->rating = ceil($suma / count($reviews));
        }else{
            $item->rating = 0;
        }
    }


    if(empty($tpl)){

        return $data;
    }
    $tpl->set('data', $data);
    $content = $tpl->fetch('view/listing/list_single.php');

    return $content;
}


function getListing($id, $userId = 0){

    if(($userId > 0)){

        if($id>0){
            $data = R::findOne('listing', ' id = ? and profile_id = ?', array($id, $userId));
        }else{
            $data = R::findOne('listing', ' profile_id = ?', array($userId));
        }


        if(!$data){
            $data = R::dispense('listing');
            $data->profile_id = $userId;
            R::store($data);
        }

        $images = R::findAll('images', ' listing_id = ? order by id asc', array($data->id));
        $imgs = array();
        foreach ($images as $image) {
            $imgs[] = $image->export();
        }
        $data->images = $imgs;

        $dates = R::findAll('listingdate', ' listing_id = ? order by id asc', array($data->id));
        $dats = array();
        foreach ($dates as $date) {
            $dats[] = $date->datum;
        }
        $data->availabledate = implode(",", $dats);

        return $data;
    }

    $data = R::findOne('listing', 'active = 1 and id = ?', array($id));
    //$data = R::load("listing", $id);

    $reviews = R::findAll('review', ' active = 1 and listing_id = ? order by id asc', array($id));
    $suma = 0;
    foreach ($reviews as $review) {
        $suma = $suma + $review->rating;
    }
    if($suma>0){
        $data->rating = ceil($suma / count($reviews));
    }else{
        $data->rating = 0;
    }

    $data->user = R::load("profile", $data->profile_id);


    return $data;

}

function getReviewListTpl($tpl, $id){

    $reviews = R::findAll('review', ' active = 1 and listing_id = ? order by id asc', array($id));

    foreach ($reviews as $review) {

        $profile = R::load("profile", $review->profile_id);
        //$user = R::findOne('user', ' id = ? ', array($review->user_id));

        $review->profile = $profile;

    }

    $tpl->set('reviews', $reviews);
    $content = $tpl->fetch('view/listing/review_list.php');

    return $content;
}

function getImages($id){

    $data = R::findAll('images', ' listing_id = ? order by id asc', array($id));

//    $tpl->set('reviews', $data);
//    $content = $tpl->fetch('view/listing/review_list.php');

    return $data;
}

function getImagesTpl($tpl, $id){

    $tpl->set("images", getImages($id));

    $content = $tpl->fetch('view/listing/single_images.php');

    return $content;
}


function getListingModelMake($tpl, $selected){

    $data = R::findAll('modelmake', '', array());
    $tpl->set("model_make", $data);

    $tpl->set("selected", $selected);

    $content = $tpl->fetch('view/listing/model/model_make.php');
    return $content;
}
function getListingModelName($tpl, $selected){
    $data = R::findAll('modelname', '', array());
    $tpl->set("model_name", $data);

    $tpl->set("selected", $selected);

    $content = $tpl->fetch('view/listing/model/model_name.php');
    return $content;
}
function getListingModelGas($tpl, $selected){
    $data = R::findAll('modelgas', '', array());
    $tpl->set("model_gas", $data);

    $tpl->set("selected", $selected);

    $content = $tpl->fetch('view/listing/model/model_gas.php');
    return $content;
}
function getListingModelYear($tpl, $selected){
    $data = R::findAll('modelyear', '', array());
    $tpl->set("model_year", $data);

    $tpl->set("selected", $selected);

    $content = $tpl->fetch('view/listing/model/model_year.php');
    return $content;
}
<?php


class BCA
{


    public static function Redirect($location)
    {
        header("Location: {$location}");
        die();
    }

    public static function Redirect301($location)
    {
        Header("HTTP/1.1 301 Moved Permanently");
        Header("Location: {$location}");
        die();
    }

}

class makeUrl{

    public static function normalize($string) {
        $match = array('Č','č','Š','š','Ž','ž');
        $replace = array('C','c','S','s','Z','z');
        $string1 = str_replace($match, $replace, $string);

        $match = array(
            "/ +/",          // double spaces
            "/\\t/",         // tabs
            "/\\r?\\n/",     // newlines
            "/^ +/",         // leading spaces
            "/ +$/"          // trailing spaces
        );
        $replace = array(
            " ",
            "",
            " ",
            "",
            ""
        );
        return preg_replace($match, $replace, $string1);
    }

    public static function makeSlug($string) {
        $string = makeUrl::normalize($string);
        $match = array('Č','č','Š','š','Ž','ž');
        $replace = array('C','c','S','s','Z','z');
        $string1 = str_replace($match, $replace, $string);
        $string1 = strtolower($string1);
        $match = array(
            "/[^a-zA-Z0-9'\"\\-\\+\\/\\,\\.\\*_\\&\\%\\$\\#\\(\\)\\=\\@\\!\\? ]/",
            "/[\\&\\_\\!\\%\\#\\$\\=\\@\\?\\+\\*\\.\\,\\/ ]/",
            "/'/",
            "/-+/",
            "/^-/",
            "/-$/"
        );
        $replace = array(
            "",
            "_",
            "",
            "-",
            "-",
            ""
        );
        return preg_replace($match, $replace, $string1);
    }
}


function setActiveClass($in){
    $class = '';
    $path = substr($_SERVER["REQUEST_URI"], 1, strlen($_SERVER["REQUEST_URI"]));

    if(is_array($in)){
        if(in_array($path, $in)){
            $class = 'active-menu';
        }
    }else{
        if($in == $path){
            $class = 'active-menu';
        }
    }
    return $class;
}

function getPart($index) {
    $parts = parse_url($_SERVER['REQUEST_URI']);
    if (!isset($parts['path'])) {
        return "/";
    }

    $pathAry = explode('/', $parts['path'], 10);
    $pathAry = array_values(array_filter($pathAry));

    if (isset($pathAry[$index])) {
        return $pathAry[$index];
    }
    return "/";
}

function handleUrlPath(){
    $ruri = getPart(0);
    return $ruri;
}

class minirouter
{
    private $p;
    private $router = [];
    function a($router, callable $c){
        $this->router[$router] = $c;
    }

    public function __construct($path)
    {
        $this->p = $path;
    }

    function e($tpl) {
        $p = $this->p;
        $k = isset($this->router[$p]) ? $this->router[$p] : $this->router[''];
        $k($tpl);
    }
}



function setHeaders(){
    if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
        $if_modified_since = preg_replace('/;.*$/', '',   $_SERVER['HTTP_IF_MODIFIED_SINCE']);
    } else {
        $if_modified_since = '';
    }

    $mtime = filemtime($_SERVER['SCRIPT_FILENAME']);
    $gmdate_mod = gmdate('D, d M Y H:i:s', $mtime) . ' GMT';


    if ($if_modified_since == $gmdate_mod) {
        header("HTTP/1.0 304 Not Modified");
        exit;
    }
    session_start();
    header("Last-Modified: $gmdate_mod");

    header('Access-Control-Max-Age: 604800');
    header('Cache-Control: public');
    header("Cache-Control: max-age=604800");
    header('Expires: ' . gmdate('D, d M Y H:i:s', time() + (604800)) . ' GMT');
}

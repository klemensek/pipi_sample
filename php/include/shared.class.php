<?php
function _t($in){


	$book = R::findOne('translation', ' country_id = ? AND obsolete = ? AND msgid = ? ', array(0=>1, 1=>0, 2=>$in));

	$str = '';
	if (count($book) > 0) {
		$str = $book->msgstring;
		$str = '' . $str;
	}else{
		$book0 = R::dispense( 'translation' );
		$book0->msgid = $in;
		$book0->msgstring = $in;
		$book0->obsolete = 0;
		$book0->country_id = 1;
		$id = R::store( $book0 );

		$str = $in;
	}

	return $str;
}

?>
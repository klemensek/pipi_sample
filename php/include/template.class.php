<?php

class template
{
    private $_data;

    public function set($name, $value)
    {
        $this->$name = $value;
    }

    public function get($name)
    {
        $value = "";
        if (isset($this->$name)) {
            $value = $this->$name;
        }
        return $value;
    }

    public function display($file)
    {
        $data = $this->_data;
        include($file);
    }

    public function fetch($file)
    {
        $data = $this->_data;
        ob_start();
        include($file);
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }

    /* cache addon */
    private $_save = false;
    public $_content = "";
    private $_key = '';

    public function save($bool)
    {
        $this->_save = $bool;
    }

    public function setKey($key)
    {
        $this->_key = $key;
    }

    public function getKey()
    {
        return $this->_key;
    }

    private $_cacheDir = "";

    public function setCacheDir($dir)
    {
        $this->_cacheDir = $dir;
    }

    public function setRequestHtml($contents)
    {
        if ($this->_save) {
            $this->_content = $this->_content . $contents;
        }
    }

    public function getRequestHtml($key)
    {

        $cache_limit_in_mins = CACHELIFETIME;
        $fileName = $this->_cacheDir . $key . '.php';

        if(REDIS_ENABLED) {
            $client = new Predis\Client();
            $value = $client->get($key);
            return unserialize($value);
        }

        if (file_exists($this->_cacheDir . "404" . $key)) {
            $fileName = $this->_cacheDir . "404" . $key . '.php';
            $secs_in_min = 60;
            $diff_in_secs = (time() - ($secs_in_min * $cache_limit_in_mins)) - filemtime($fileName);
            if ($diff_in_secs < 0) {
                return file_get_contents($fileName);
            }
        } else if (file_exists($fileName)) {
            $secs_in_min = 60;
            $diff_in_secs = (time() - ($secs_in_min * $cache_limit_in_mins)) - filemtime($fileName);
            if ($diff_in_secs < 0) {
                return file_get_contents($fileName);
            }
        }
    }

    public function saveRequestHtml($key)
    {

        if (!$this->_save) {
            return;
        }

        if(REDIS_ENABLED){
            $client = new Predis\Client();
            $client->set($key, serialize($this->_content));
            $client->expire($key, CACHELIFETIME*60);
        }else {

            $fileName = $this->_cacheDir . $key . '.php';
            $file = fopen($fileName, 'w');
            fwrite($file, $this->_content);
            fclose($file);
        }
    }

    public function showRequestHtml($key)
    {
        if (!$this->_save) {
            return;
        }
        if (!empty($key)) {
            $this->_key = $key;
        } else {
            die('what?');
        }

        if ($content = $this->getRequestHtml($key)) {
            echo $content;
            timer_ends();
            echo '<!--' . (timer_calc()) . '-->';
            die();
        }
    }

    public function stop()
    {
        $this->saveRequestHtml($this->_key);
    }
}

?>
<?php
session_start();
header ('Content-type: text/html; charset=utf-8');
error_reporting (E_ALL);


define("__LOCALURL", 'http://www.pipisample.si');
define('__CMS_ROOT__', dirname(__FILE__) . '/../');

define('IMAGES_ORIGINAL', "/userfiles/images/listing/");
define('IMAGES_THUMB', "/userfiles/images/listing/thumbnail/");
define('IMAGES_MEDIUM', "/userfiles/images/listing/medium/");

//define('CDN', 'http://garage-962d.kxcdn.com');
define('CDN', '');

date_default_timezone_set('Europe/Ljubljana');


require_once(__CMS_ROOT__.'php/conf.db.php');
require_once(__CMS_ROOT__.'vendor/autoload.php');
require_once(__CMS_ROOT__.'php/include/function.router.php');
require_once(__CMS_ROOT__.'php/include/function.listing.php');
require_once(__CMS_ROOT__.'php/include/shared.class.php');
require_once(__CMS_ROOT__.'php/include/template.class.php');
require_once(__CMS_ROOT__.'php/include/rb.php');


R::setup('mysql:host='.DB_SERVER.';dbname='.DB_DATABASE, DB_USERNAME, DB_PASSWORD);
R::freeze(false);



?>
-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2017 at 01:25 PM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pipisample`
--

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) UNSIGNED NOT NULL,
  `listing_id` int(11) UNSIGNED DEFAULT NULL,
  `url` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `listing_id`, `url`) VALUES
(1, 1, 'suzuki__baleno_1_6_glx_original_only_30_tkm_2001_1_lgw.jpg'),
(2, 1, 'suzuki-baleno-16-gti-2001-16668700.jpg'),
(3, 1, 'suzuki-baleno-2001-8439818.png'),
(4, 1, 'suzuki-baleno-glxse-06-image-size-800-x-600-px-imagejpeg-20785_29049.jpg'),
(5, 1, 'Suzuki-Baleno-II-Sedan-1.6i-M-Green-NEWM00592-4.jpg'),
(6, 1, 'Suzuki-Baleno-II-Sedan-1.6i-M-Green-NEWM00592-8.jpg'),
(7, 2, '1024px-2009_Audi_A3_(8PA)_2.0_TDI_5-door_Sportback_(2011-04-02)_02.jpg'),
(8, 2, '1024px-2010_Acura_TSX_--_03-15-2010.jpg'),
(9, 2, '1024px-2010_Hyundai_ix35_(LM)_Elite_wagon_02.jpg'),
(10, 2, '1024px-Audi_s3_front.jpg'),
(11, 3, '1024px-BMW_E90_Edition10_AME.jpg'),
(12, 3, '1024px-BMW_E92_M3.JPG'),
(13, 3, '1024px-BMW_E92_M3_Heck.JPG'),
(14, 4, 'Citroen_AX_red_hl.jpg'),
(15, 4, 'CITROENAX3Doors-798_6 (1).jpg'),
(16, 4, 'CITROENAX3Doors-798_6.jpg'),
(17, 4, 'citroen-ax-gti.jpg'),
(18, 4, 'prenos.jpg'),
(19, 5, '2017-Fiesta-ST-Orange-Spice.png'),
(20, 5, 'cq5dam.web.768.768 (1).jpeg'),
(21, 5, 'cq5dam.web.768.768.jpeg'),
(22, 5, 'ford-fiesta.jpg'),
(23, 5, 'ford-fiesta-zetec-16-06.jpg'),
(24, 6, '13Peu208Gti3drWhiFR1_800.jpg'),
(25, 6, '16Peu208GtLin5drSilFR1_800 (1).jpg'),
(26, 6, '16Peu208GtLin5drSilFR1_800.jpg'),
(27, 6, '208-5p-2.jpg'),
(28, 6, 'peugeot20812lpuretech821215.jpg'),
(29, 6, 'peugeot-argentina-208-energia.291533.43.jpg'),
(30, 7, '07-renault-clio-artic-white.png'),
(31, 7, '15RenCliDyn5drWhiFR1_800.jpg'),
(32, 7, '16RenCliRsTroNav5drWhiFL1_800.jpg'),
(33, 7, 'renault-clio-rs-mona_600x0w.jpg'),
(34, 8, 'skoda-rapid_story_647_110316121421.jpg'),
(35, 8, 'skoda-rapid-candy-white.png'),
(36, 8, 'Slider3.jpg'),
(37, 9, '2656dab0dfb14308b2891c4759bd65f4.jpg'),
(38, 9, 'Avto_fokus_Volkswagen_polo9_b.jpg'),
(39, 9, 'Volkswagen-Polo-Exterior-95634.jpg'),
(40, 9, 'volkswagen-polo-reflex-silver.png'),
(41, 9, 'VW_Polo_1.2_TSI_BlueMotion_Technology_Fresh_(V,_Facelift)_–_Frontansicht,_13._Juli_2014,_Velbert.jpg'),
(48, 1, '5b490391756fee3ab90a3602471907d8.jpg'),
(52, 1, 'fiat-panda-4x4-road-test-13.jpg'),
(60, 1, 'fiat_panda_4x4_1024x768^640x480^.jpg'),
(61, 1, 'ccc6840ad28a5c5a4458619651a32fb9--marbella-pandas.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `listing`
--

CREATE TABLE `listing` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` int(11) UNSIGNED DEFAULT NULL,
  `address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_id` int(11) UNSIGNED DEFAULT NULL,
  `model_make` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model_gas` int(11) UNSIGNED DEFAULT NULL,
  `model_year` int(11) UNSIGNED DEFAULT NULL,
  `addtags` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lng` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `listing`
--

INSERT INTO `listing` (`id`, `title`, `model`, `desc`, `region`, `address`, `phone`, `email`, `website`, `video`, `profile_id`, `model_make`, `model_gas`, `model_year`, `addtags`, `price`, `lat`, `lng`, `active`, `model_name`) VALUES
(1, 'suzuki', '', 'Discribe the listing', 0, '20 Laknerjeva ulica, Slovenia', 'Listing Phone', 'Listing Email', 'sting Website', 'video', 1, 'Suzuki', 201, 2001, '', '10', '46.0703486', '14.470239499999934', 1, 'Baleno'),
(2, 'audi, ', NULL, 'audi kot se šika', 14, '140 Celovška cesta, Slovenia', '031031031', 'email@gmail.com', '', '', 1, 'Audi', 202, 2008, 'nov, radio, CDji', '20', '46.0724344', '14.48771069999998', 1, 'A2'),
(3, 'bmw', NULL, 'bmw kot nov, samo za frende', 1, '100 Celovška cesta, Slovenia', '064987987', 'bmw@audi.si', '', '', 1, 'BMW', 205, 2012, 'veter, sonce, senca', '50', '46.0682105', '14.491397600000028', 1, 'serija 4:'),
(4, 'star citroen', NULL, 'če imaš dodatno tzavarovanje, ni panike', 14, 'Šišenska cesta 1000, Slovenia', '0987654987', 'ax@citren.com', '', '', 1, 'Citroen', 201, 1990, 'rdeč, super, počasen', '15', '46.0711175', '14.480377900000008', 1, 'AX'),
(5, 'moj ford, samo zate', NULL, 'moj ford, samo zate moj ford, samo zate', 14, '20 Litostrojska Cesta, Slovenia', '65473214', 'asdf@sdf.com', '', '', 1, 'Ford', 202, 2008, 'moj ford, samo zate', '20', '46.0754943', '14.487715600000001', 1, 'Tourneo'),
(6, 'peuto', NULL, 'Discribe the listing', 3, 'Goriška ulica, Ljubljana, Slovenia', '654321454', 'sdfg@sdgf.com', '', '', 1, 'Peugeot', 202, 2016, 'Discribe, the, listing', '12', '46.07243039999999', '14.494543499999963', 1, '1007'),
(7, 'renault', NULL, 'iz slovenija', 0, '80 Celovška cesta, Slovenia', '6546548764', 'asdf@sgf.cvom', '', '', 1, 'Renault', 201, 2015, '987, 987, 987, 98,7 ', '123', '46.0663095', '14.493214099999932', 1, 'Clio'),
(8, 'skoda', NULL, 'škoda, ceski jaguar', 1, 'Rašiška ulica, Slovenia', '2345234545', 'sdfgsdfg@fg.com', '', '', 1, 'Škoda', 203, 2010, '', '50', '46.07296350000001', '14.479908099999989', 1, 'Rapid'),
(9, 'volkswagen', NULL, 'tvoj polo', 1, '10 Litostrojska Cesta, Slovenia', '06547891', 'asdf3@gf.si', '', '', 1, 'Volkswagen', 202, 2010, 'polo', '50', '46.0743249', '14.487926000000016', 1, 'Polo'),
(10, 'BMW X1', NULL, '', 0, '', '', '', '', '', 2, '', 0, 0, '', '35', '', '', 0, 'serija X1:'),
(11, '', NULL, '', 0, '', '', '', '', '', 5, 'Volkswagen', 202, 2013, '', '', '', '', 1, 'Golf');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) UNSIGNED NOT NULL,
  `fb_access_token` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fb_id` bigint(11) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phoneprofile` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aboutyou` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedinurl` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebookurl` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitterurl` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtubeurl` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_registered` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `fb_access_token`, `fb_id`, `name`, `email`, `first_name`, `last_name`, `gender`, `picture`, `phoneprofile`, `aboutyou`, `linkedinurl`, `facebookurl`, `twitterurl`, `youtubeurl`, `birth`, `date_registered`) VALUES
(1, '', 10155110641159611, 'Primož Klemenšek', 'klemensek@gmail.com', 'Primož', 'Klemenšek', 'male', 'https://scontent.xx.fbcdn.net/v/t1.0-1/s320x320/24852104_10155364349499611_3954355398892723087_n.jpg?oh=5df919b80c40bf617f61162f885fb9e4&oe=5ABBF1E2', '', '', '', '', '', '', NULL, NULL),
(2, '', 10214238852388881, 'Urša Hribernik', 'wursha@gmail.com', 'Urša', 'Hribernik', 'female', 'https://scontent.xx.fbcdn.net/v/t1.0-1/s320x320/18425312_10213003477825289_3017352038858442736_n.jpg?oh=85c7ecce1b60d2cdaae0c88b75428bc0&oe=5ACFE8F8', '+38641950532', 'zelo fest dicla ;)', '', '', '', '', '09/19/1988', NULL),
(3, '', 1021, 'k', 'k@gmail.com', 'k', 'h', 'female', '', '', '', '', '', '', '', '09/01/2017', NULL),
(4, '', 1020, 't', 't@hotmail.com', 't', 'm', 'male', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-24'),
(5, '', 1015, 'u', 'z@gmail.com', 'u', 'z', 'male', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-03'),
(6, '', 1021, 'b', 'b@gmail.com', 'b', 'l', 'female', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-19');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) UNSIGNED NOT NULL,
  `rating` int(11) UNSIGNED DEFAULT NULL,
  `content` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_id` int(11) UNSIGNED DEFAULT NULL,
  `profile_id` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `rating`, `content`, `listing_id`, `profile_id`, `active`) VALUES
(1, 3, 'jehe', 2, 2, 0),
(2, 4, 'Kul avto', 10, 1, 0),
(3, 3, 'krneki', 9, 2, 1),
(4, 4, 'ali lahko dvakrat komentiram', 9, 2, 1),
(5, 5, 'kaj pa brez zvezdic', 9, 2, 1),
(6, 5, 'brez zvezdic je bug, ker vrne 5 zvezdic v commentu', 9, 2, 1),
(7, 1, 'pa zgori zvezdice se ne delajo', 9, 2, 1),
(8, 1, 'ocitno delajo, kaksen je algoritem za povprecje?', 9, 2, 1),
(9, 3, '', 2, 2, 1),
(10, 5, 'brez commenta ne sme delat..review mora biti vsaj 120 znakov', 2, 2, 1),
(11, 3, 'Naj', 1, 6, 1),
(12, 1, 'Slabo', 1, 6, 1),
(13, 5, '', 1, 6, 1),
(14, 3, 'how', 7, 2, 1),
(15, 5, 'jsjs', 1, 2, 1),
(16, 5, 'hsskk', 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `translation`
--

CREATE TABLE `translation` (
  `id` int(11) UNSIGNED NOT NULL,
  `msgid` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `msgstring` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `obsolete` int(11) UNSIGNED DEFAULT NULL,
  `country_id` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `translation`
--

INSERT INTO `translation` (`id`, `msgid`, `msgstring`, `obsolete`, `country_id`) VALUES
(1, 'link_search', 'link_search', 0, NULL),
(2, 'link_search', 'link_search', 0, NULL),
(3, 'link_search', 'link_search', 0, NULL),
(4, 'Blizu', 'Blizu', 0, NULL),
(5, 'Lokacija', 'Lokacija', 0, NULL),
(6, 'Od', 'Od', 0, NULL),
(7, 'Do', 'Do', 0, NULL),
(8, 'Išči', 'Išči', 0, NULL),
(9, 'link_search', 'link_search', 0, NULL),
(10, 'link_search', 'link_search', 0, NULL),
(11, 'link_search', 'link_search', 0, 1),
(12, 'Blizu', 'Blizu', 0, 1),
(13, 'Lokacija', 'Lokacija', 0, 1),
(14, 'Od', 'Od', 0, 1),
(15, 'Do', 'Do', 0, 1),
(16, 'Išči', 'Išči', 0, 1),
(17, 'link_single', 'link_single', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_foreignkey_images_listing` (`listing_id`);

--
-- Indexes for table `listing`
--
ALTER TABLE `listing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_foreignkey_listing_profile` (`profile_id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_foreignkey_profile_fb` (`fb_id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_foreignkey_review_listing` (`listing_id`),
  ADD KEY `index_foreignkey_review_profile` (`profile_id`);

--
-- Indexes for table `translation`
--
ALTER TABLE `translation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_foreignkey_translation_country` (`country_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `listing`
--
ALTER TABLE `listing`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `translation`
--
ALTER TABLE `translation`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `c_fk_images_listing_id` FOREIGN KEY (`listing_id`) REFERENCES `listing` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `listing`
--
ALTER TABLE `listing`
  ADD CONSTRAINT `c_fk_listing_profile_id` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `c_fk_review_listing_id` FOREIGN KEY (`listing_id`) REFERENCES `listing` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `c_fk_review_profile_id` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<!-- BANNER SECTION -->
<section class="clearfix homeBanner" style="background-image: url(/img/banner/pexels-photo-167890.jpeg);">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="banerInfo">
                    <h1>Najemi sosedov avto in odpipkaj v neznano!</h1>
                    <p>Hitro, enostavno, ugodno!</p>
                    <form class="form-inline" action="/<?php echo _t("link_search"); ?>" method="get">


                        <div class="row">

                            <div class="col-lg-9 col-md-12 col-sm-12">

                                <div class="" style="background:rgba(180, 180, 180, 0.3); padding: 20px;">

                                    <div class="row">
                                        <div class=" col-sm-8 col-xs-12">

                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><?php echo _t("Blizu"); ?></div>
                                                    <input type="text" class="form-control" id="nearLocation"
                                                           name="location"
                                                           placeholder="<?php echo _t("Lokacija"); ?>">
                                                    <div class="input-group-addon addon-right"><i
                                                                class="icon-listy icon-target"
                                                                aria-hidden="true"></i></div>
                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                    <div class="row">
                                        <div class="col-lg-1">
                                            &nbsp
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="sc-date-field startDate">
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <div class="input-group date ed-datepicker filterDate">
                                                        <input type="text" value="" autocomplete="off" name="datefrom"
                                                               class="pickdate form-control setDate"
                                                               placeholder="<?php echo _t("Od"); ?>">
                                                        <div class="input-group-addon addon-right">
                                                            <span class="fa fa-calendar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <div class="input-group date ed-datepicker filterDate">
                                                        <select id="time_pickup" name="timefrom"
                                                                class="form-control selectboxdiv selectpicker">
                                                            <?php
                                                            $active = "";
                                                            for($hours=0; $hours<24; $hours++) {
                                                                for ($mins = 0; $mins < 60; $mins += 30) {
                                                                    $time = str_pad($hours, 2, '0', STR_PAD_LEFT) . ':'
                                                                        . str_pad($mins, 2, '0', STR_PAD_LEFT);
                                                                    $active = ($time == date("H")) ? 'selected="selected" class="active"' : null;
                                                                    echo '<option ' . $active . ' >' . $time . '</option>';
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                        <div class="input-group-addon addon-right">
                                                            <span class="fa fa-clock-o"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="sc-date-field endDate">

                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <div class="input-group date ed-datepicker filterDate">
                                                        <input type="text" value="" autocomplete="off" name="dateto"
                                                               class="dropdate form-control setDate"
                                                               placeholder="<?php echo _t("Do"); ?>">
                                                        <div class="input-group-addon addon-right">
                                                            <span class="fa fa-calendar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <div class="input-group date ed-datepicker filterDate">
                                                        <select id="time_dropoff" name="timeto"
                                                                class="form-control selectboxdiv selectpicker">
                                                            <?php
                                                            $active = "";
                                                            for($hours=0; $hours<24; $hours++) {
                                                                for ($mins = 0; $mins < 60; $mins += 30) {
                                                                    $time = str_pad($hours, 2, '0', STR_PAD_LEFT) . ':'
                                                                        . str_pad($mins, 2, '0', STR_PAD_LEFT);
                                                                    $active = ($time == date("H")) ? 'selected="selected" class="active"' : null;
                                                                    echo '<option ' . $active . ' >' . $time . '</option>';
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                        <div class="input-group-addon addon-right">
                                                            <span class="fa fa-clock-o"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-1">
                                            &nbsp
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn btn-primary pull-right">
                                                <?php echo _t("Išči"); ?> <i
                                                        class="fa fa-search"
                                                        aria-hidden="true"></i></button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

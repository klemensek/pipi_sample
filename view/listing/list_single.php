<div class="listContent">
    <div class="row">
        <?php
        foreach ($this->data as $data) {
            ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="thingsBox">
                    <a href="/<?php echo _t("link_single"); ?>/<?php echo $data->id; ?>">
                        <div class="thingsImage">
                            <img src="<?php echo IMAGES_MEDIUM.$data->images["url"]; ?>" alt="<?php echo $data->title; ?>">
                            <div class="thingsMask">
                                <ul class="list-inline rating">
                                    <li><i class="fa <?php echo ($data->rating >= 1) ? 'fa-star' : 'fa-star-o'; ?>"
                                           aria-hidden="true"></i></li>
                                    <li><i class="fa <?php echo ($data->rating >= 2) ? 'fa-star' : 'fa-star-o'; ?>"
                                           aria-hidden="true"></i></li>
                                    <li><i class="fa <?php echo ($data->rating >= 3) ? 'fa-star' : 'fa-star-o'; ?>"
                                           aria-hidden="true"></i></li>
                                    <li><i class="fa <?php echo ($data->rating >= 4) ? 'fa-star' : 'fa-star-o'; ?>"
                                           aria-hidden="true"></i></li>
                                    <li><i class="fa <?php echo ($data->rating >= 5) ? 'fa-star' : 'fa-star-o'; ?>"
                                           aria-hidden="true"></i></li>
                                </ul>
                                <h2>
                                    <?php echo $data->title; ?>
                                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                                </h2>
                                <p><?php echo $data->city; ?> <span class="placeName"></span></p>
                                <p><?php echo $data->desc; ?></p>
                            </div>
                        </div>
                        <div class="thingsCaption ">
                            <ul class="list-inline captionItem">
                                <?php $features = explode(", ", $data->addtags);
                                foreach ($features as $feature) { ?>
                                    <li><i class="fa fa-check-circle-o" aria-hidden="true"></i> <?php echo $feature; ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </a>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
